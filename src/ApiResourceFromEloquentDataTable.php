<?php

namespace Dottystyle\Laravel\DataTables;

use Yajra\DataTables\EloquentDataTable;

class ApiResourceFromEloquentDataTable extends EloquentDataTable
{
    use TransformsToResource;
}