<?php

namespace Dottystyle\Laravel\DataTables;

use Yajra\DataTables\CollectionDataTable;

class ApiResourceFromCollectionDataTable extends CollectionDataTable
{
    use TransformsToResource;
}