<?php

namespace Dottystyle\Laravel\DataTables;

interface ProvidesResourceClassTransformer
{
    /**
     * Get the resource class (must extend Illuminate\Http\Resources\Json\Resource or 
     * Illuminate\Http\Resources\Json\ResourceCollection) to use as transformer.
     * 
     * @return string
     */
    public function getResourceClassTransformer();
}