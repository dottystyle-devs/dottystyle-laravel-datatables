<?php

namespace Dottystyle\Laravel\DataTables;

use Yajra\DataTables\Utilities\Request as BaseRequest;

class Request extends BaseRequest
{
    /**
     * @var array
     *   An array of column names that can be searched on.
     */
    protected $searchableColumns = [];

    /**
     * Set the searchable columns.
     * 
     * @param array|...string $columns
     * @return void
     */
    public function setSearchableColumns($columns)
    {
        if (! is_array($columns)) {
            $columns = func_get_args();
        }

        $this->searchableColumns = array_merge($this->searchableColumns, $columns);
    }

    /**
     * Get the list of searchable columns.
     * 
     * @return array
     */
    public function getSearchableColumns()
    {
        return $this->searchableColumns;
    }

    /**
     * @inheritdoc
     */
    public function isColumnSearchable($i, $columnSearch = true)
    {
        // Check first if column has been explicitly set as searchable.
        if (! in_array($this->columnName($i), $this->searchableColumns)) {
            return false;
        }

        return parent::isColumnSearchable($i, $columnSearch);
    }

    /**
     * Get column index by its name.
     * Returns -1 if not found.
     * 
     * @param string $name
     * @return int
     */
    public function columnIndex($name)
    {
        foreach ($this->searchableColumnIndex() as $index) {
            if ($name === $this->columnName($index)) {
                return $index;
            }
        }

        return -1;
    }
}