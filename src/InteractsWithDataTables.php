<?php

namespace Dottystyle\Laravel\DataTables;

use UnexpectedValueException;

trait InteractsWithDataTables
{
    /**
     * Set the searchable columns for the next usage of datatables.
     * 
     * @param array|...string $columns
     * @return void
     */
    public function setSearchableColumns($columns)
    {
       $this->getDataTableRequest()->setSearchableColumns($columns);
    }

    /**
     * Get the current datatable request.
     * 
     * @return \Yajra\DataTables\Utilities\Request
     */
    protected function getDataTableRequest()
    {
        $request = app('datatables.request');

        if (! $request instanceof Request) {
            throw new UnexpectedValueException(
                sprintf('Request must be an instance of %s', Request::class)
            );
        } 

        return $request;
    }
}