<?php

namespace Dottystyle\Laravel\DataTables;

use Illuminate\Database\Query\Builder;
use Yajra\DataTables\QueryDataTable;

class ApiResourceFromQueryDataTable extends QueryDataTable
{
    use TransformsToResource;
}