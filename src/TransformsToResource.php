<?php

namespace Dottystyle\Laravel\DataTables;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Yajra\DataTables\Exceptions\Exception as DataTableException;
use InvalidArgumentException;

trait TransformsToResource
{
    /**
     * @var mixed 
     */
    protected $resourceCollectionTransformer;

    /**
     * Set the transformer to resource collection.
     * 
     * @param string|callable $transformer
     * @return $this
     */
    public function setResourceCollectionTransformer($transformer)
    {
        $this->resourceCollectionTransformer = $transformer;

        return $this;
    }

    /**
     * Transform output.
     *
     * @param mixed $results
     * @param mixed $processed
     * @return array
     */
    protected function transform($results, $processed)
    {
        // Resolve the resource transformer on the container.
        // Resolved value must be an instance of Illuminate\Http\Resources\Json\Resource.
        if (is_string($this->resourceCollectionTransformer) && class_exists($this->resourceCollectionTransformer)) {
            return $this->transformResultsUsingResource($this->resourceCollectionTransformer, $results);
        } else if (is_callable($this->resourceCollectionTransformer)) {
            return call_user_func($this->resourceCollectionTransformer, $results, $this->request);
        } 
        
        // If transformer is neither a valid class or a callable and the first element of the collection is 
        // resourceable, transform the collection using
        // use the assigned resource class as the transformer
        $results = collect($results);

        if ($results->isNotEmpty() && $results->first() instanceof ProvidesResourceClassTransformer) {
            return $this->transformResultsUsingResource($results->first()->getResourceClassTransformer(), $results);
        } else {
            return $results->toArray();
        }

        throw new DataTableException("Resource collection transformer is not set");
    }

    /**
     * Get the resource collection instance given its class name.
     * 
     * @param string $class
     * @param array $results
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    protected function transformResultsUsingResource($class, $results)
    {
        // Here we will determine the correct instance of resource to transform the results.
        if (is_subclass_of($class, ResourceCollection::class)) {
            $transformer = new $class($this->resultsToTransform($results));
        } else if (is_subclass_of($class, JsonResource::class)) {
            $transformer = $class::collection($this->resultsToTransform($results));
        }

        if (!($transformer instanceof JsonResource)) {
            throw new InvalidArgumentException(sprintf('Transformer must be an instance of %s', JsonResource::class));
        }

        return $transformer->resolve($this->request);
    }

    /**
     * Do anything on the results before the transformation.
     * 
     * @param array $results
     * @return \Illuminate\Http\Resources\Json\ResourceCollection;
     */
    protected function resultsToTransform($results)
    {
        return $results;
    }
}