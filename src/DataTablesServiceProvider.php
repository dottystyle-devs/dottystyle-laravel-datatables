<?php

namespace Dottystyle\Laravel\DataTables;

use Illuminate\Support\ServiceProvider;
use InvalidArgumentException;

class DataTablesServiceProvider extends ServiceProvider
{
    /**
     * 
     * @return void
     */
    public function boot()
    {
        // Override the datatables request instance
        $this->app->extend('datatables.request', function ($request) {
            return new Request;
        });

        $dataTables = $this->app['datatables'];

        // Register custom datatable engines
        $engines = [
            'resourceFromQuery' => ApiResourceFromQueryDataTable::class,
            'resourceFromEloquent' => ApiResourceFromEloquentDataTable::class,
            'resourceFromCollection' => ApiResourceFromCollectionDataTable::class
        ];

        foreach ($engines as $engine => $class) {
            if (method_exists(get_class($dataTables), $engine) || $dataTables::hasMacro($engine)) {
                continue;
            }

            $dataTables->macro($engine, function () use ($class) {
                if (! call_user_func_array([$class, 'canCreate'], func_get_args())) {
                    throw new InvalidArgumentException();
                }

                return call_user_func_array([$class, 'create'], func_get_args());
            });
        }
    }

    /**
     * Register services to the container.
     * 
     * @return void
     */
    public function register() {}
}